import cgi

import application.models.user as AppUser

from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#

class MainPage(webapp.RequestHandler):
  def get(self):
      if not AppUser.User(mail="filippo.baruffaldi@gmail.com").GetByEmail():
          AppUser.User(mail="filippo.baruffaldi@gmail.com", rank=2).put()
          print 'hohoho'
      print 'lalala'
    
class Logout(webapp.RequestHandler):
  def get(self):
      url = self.request.get('requrl', default_value="/")
      self.response.out.write(template.render('templates/auth.html', {'link': users.create_logout_url(url),
                                                                      'name': users.get_current_user().nickname(),
                                                                      'mail': users.get_current_user().email()}))

class Login(webapp.RequestHandler):
  def get(self):
      url = self.request.get('requrl', default_value="/")
      self.response.out.write(template.render('templates/auth.html', {'link': users.create_login_url(url)}))

#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#

application = webapp.WSGIApplication([('/logout', Logout),
                                      ('/login', Login),
                                      ('/.*', MainPage)],
                                     debug=True)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()