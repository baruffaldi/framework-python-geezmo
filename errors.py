import cgi

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#
	
	
class ErrorHandler(webapp.RequestHandler):
  def get(self):
    self.response.out.write("""
	<html><body>
		<h2>Computer Shopping 3 :: DataCenter - Error</h2>
		<p><b>Error</b>: the page you request does not exists.</p>
		<hr />
		<pre>
	It is said, "To err is human"
	That quote from alt.times.lore,
	Alas, you have made an error,
	So I say, "404."

	Double-check your URL,
	As we all have heard before.
	You ask for an invalid filename,
	And I respond, "404."

	Perhaps you made a typo --
	Your fingers may be sore --
	But until you type it right,
	You'll only get 404.

	Maybe you followed a bad link,
	Surfing a foreign shore;
	You'll just have to tell that author
	About this 404.

	I'm just a lowly server
	(Who likes to speak in metaphor),
	So for a request that Idon't know,
	I must return 404.

	Be glad I'm not an old mainframe
	That might just dump its core,
	Because then you'd get a ten-meg file
	Instead of this 404.

	I really would like to help you,
	But I don't know what you're looking for,
	And since I don't know what you want,
	I give you 404.

	Remember Poe, insane with longing
	For his tragically lost Lenore.
	Instead, you quest for files.
	Quoth the Raven, "404!"
		</pre>
	</body></html>
	""")
	
#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#

application = webapp.WSGIApplication([('/.*', ErrorHandler),
									  ('/error.*', ErrorHandler)],
                                     debug=True)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()