import os
import application.library.json as Json

import application.models.video as AppVideo
import application.models.playlist as AppPlaylist

from urllib import urlencode
from django.utils import simplejson
from google.appengine.ext import db
from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#
            

class GetAtomUsers(webapp.RequestHandler):
    def get(self):
        if not users.get_current_user() or users.get_current_user() and not AppUser.User(mail=users.get_current_user().email()).GetByEmail(): self.redirect('/login?'+urlencode({'requrl':os.environ["PATH_INFO"]+"?"+os.environ["QUERY_STRING"]}), True)
        self.response.headers["Content-Type"] = 'text/xml'
        userz = AppUser.User.all()
        for user in userz:
            self.response.out.write(AppUser.User.to_xml(user))
            

class MainPage(webapp.RequestHandler):
  def get(self):
    if not users.get_current_user() or users.get_current_user() and not AppUser.User(mail=users.get_current_user().email()).GetByEmail(): self.redirect('/login?'+urlencode({'requrl':os.environ["PATH_INFO"]+"?"+os.environ["QUERY_STRING"]}), True)
    self.response.out.write("""
    <html><body>
        <h2>Damiani Fashion :: DataCenter</h2>
        <p><b>DataCenter</b>: ready</p>
    </body></html>""")
    
    
class ErrorHandler(webapp.RequestHandler):
  def get(self):
    self.response.out.write("""
    <html><body>
        <h2>Damiani Fashion :: DataCenter - Error</h2>
        <p><b>Error</b>: the feed you request does not exists.</p>
        <hr />
        <pre>
    It is said, "To err is human"
    That quote from alt.times.lore,
    Alas, you have made an error,
    So I say, "404."

    Double-check your URL,
    As we all have heard before.
    You ask for an invalid filename,
    And I respond, "404."

    Perhaps you made a typo --
    Your fingers may be sore --
    But until you type it right,
    You'll only get 404.

    Maybe you followed a bad link,
    Surfing a foreign shore;
    You'll just have to tell that author
    About this 404.

    I'm just a lowly server
    (Who likes to speak in metaphor),
    So for a request that Idon't know,
    I must return 404.

    Be glad I'm not an old mainframe
    That might just dump its core,
    Because then you'd get a ten-meg file
    Instead of this 404.

    I really would like to help you,
    But I don't know what you're looking for,
    And since I don't know what you want,
    I give you 404.

    Remember Poe, insane with longing
    For his tragically lost Lenore.
    Instead, you quest for files.
    Quoth the Raven, "404!"
        </pre>
    </body></html>
    """)


#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#

if os.environ["REMOTE_ADDR"] == "127.0.0.1":
    Debug = True
else: Debug = False

application = webapp.WSGIApplication([('/feeds', MainPage),
                                      ('/feeds/', MainPage),
                                      
                                      ('/feeds/users-atom', GetAtomUsers),
                                      
                                      ('/feeds/.*', ErrorHandler)
                                      ], Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()