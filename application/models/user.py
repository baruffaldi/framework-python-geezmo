from google.appengine.ext import db

#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#

class User(db.Model):
    date = db.DateTimeProperty(auto_now_add=True)
    
    mail = db.EmailProperty(required=True)
    rank = db.IntegerProperty(required=False)
    
    def GetByEmail(self):
        ret = None
        user = db.GqlQuery("SELECT * FROM User WHERE mail = '"+self.mail+"'").fetch(1) 
        if user:
            ret = user
        
        return ret
    
#------------------------------------------------------------------------------------------------------#
#------------------------------------------------------------------------------------------------------#