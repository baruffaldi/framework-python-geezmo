import datetime

def dumprow(r):
    a = {}
    for k in r.fields().keys():
        v = getattr(r, k)
        a['id'] = str(r.key().id())
        a['key'] = str(r.key())
        if isinstance(v, str):
            a[k] = str(v)
        elif isinstance(v, unicode):
            a[k] = str(v)
        elif isinstance(v, datetime.datetime):
            a[k] = v.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(v, datetime.date):
            a[k] = v.strftime('%Y-%m-%d')
        elif isinstance(v, datetime.time):
            a[k] = v.strftime('%H:%M:%S')
        elif isinstance(v, (int, float, list)):
            a[k] = v
        else:
            a[k] = str(v)
    
    return a

def dumpquery(query):
    s = []
    for r in query:
        s.append(dumprow(r))
    return s
